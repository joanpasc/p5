'use strict'

const port = process.env.PORT || 4000; 
const express = require('express'); 
const logger = require('morgan');
const mongojs = require('mongojs'); 
const fs=require('fs');

const PassService = require('./services/pass.service');
const TokenService = require('./services/token.service');
const moment= require('moment');

const app = express(); 
const https = require('https');
const OPTIONS_HTTPS ={
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};



var db = mongojs("SD-p5");
var id = mongojs.ObjectID;

var helmet = require('helmet');
const { resolveSoa } = require('dns');
const req = require('express/lib/request');
const res = require('express/lib/response');
const { unix } = require('moment');

const cors = require('cors'); 
// Middlewares
var allowCrossTokenHeader = (req, res, next) => { 
 res.header("Access-Control-Allow-Headers", "*"); 
return next(); 
}; 
var allowCrossTokenOrigin = (req, res, next) => { 
 res.header("Access-Control-Allow-Origin", "*"); 
return next(); 
}; 
app.use(cors()); 
app.use(allowCrossTokenHeader); 
app.use(allowCrossTokenOrigin); 

var auth = (req, res, next) => {

    const token =req.headers.authorization.split(' ')[1];
    TokenService.decodificaToken(token).then( userId => { 
        req.user={
            id: userId.sub,
            token: token
        }
        return next();
    }).catch(err =>
        res.status(400).json({
            result: 'ko',
            msg: err
        })
    );
}

// Declaramos los middleware 
app.use(logger('dev')); // probar con: tiny, short, dev, common, combined

//Middleware
app.use(helmet());
app.use(express.urlencoded({extended:false}));
app.use(express.json());

//_______________auth______________________________

app.post('/api/auth',(req, res, next) => { 
    const elemento = req.body; 
    console.log(elemento);
  
    if (!elemento.email) { 
      res.status(400).json ({ 
      result:'KO',
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <emial>' 
      }); 
    } else if(!elemento.pass) { 
      res.status(400).json ({ 
        result:'KO',
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <pass>' 
      }); 
    }else { 
      db.user.findOne({ email: elemento.email }, (err, usuario)=>{
        if(err) return next(err);
        if(!usuario ){
            res.status(400).json({
                result:'KO',
                error: 'User not found',
                description: 'EL usuario no está registrado en la base de datos'
            });
        }else{
            PassService.comparaPassword(elemento.pass, usuario.pass)
            .then(val => {
                if(val){
                  db.user.update({email: elemento.email}, {$set: {lastLogin: moment().unix()}}, {safe: true, multi: false}, (err, elementoModif) =>{
                    if(err) return next(err);
                    const ctoken = TokenService.creaToken(usuario);
                    res.json({
                        result: 'OK',
                        user: usuario,
                        token: ctoken
                    });
                });
                }else{
                  res.json({
                    result: 'NOT OK',
                    description:'La contraseña no coincide'
                });
                }  
            });
        }
    })
    } 
  }); 
  
  
  
  app.post('/api/auth/reg', (req, res, next) => { 
    const elemento = req.body; 
    console.log(elemento);
  

    if (!elemento.nombre) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <nombre>' 
      }); 
    } else if(!elemento.email) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <email>' 
      }); 
    }else if(!elemento.pass) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <pass>' 
      }); 
    } else { 
      db.user.findOne({ email: elemento.email }, (err, usuario)=>{
        if(usuario){
            res.status(400).json({error: 'Duplicated User',
            description: 'EL usuario que se ha querido registrar ya existe'});
        }else{
            PassService.encriptaPassword(elemento.pass)
            .then(passEnc => {
               
               const usuario = {
                    email: elemento.email,
                    name: elemento.nombre,
                    pass: passEnc,
                    signUpDate: moment().unix(),
                    lastLogin: moment().unix()
                };
                db.user.save(usuario, (err, coleccionGuardada) => { 
                    if(err) return next(err);
                    const ctoken = TokenService.creaToken(coleccionGuardada);
                    res.json({
                        result: 'OK',
                        user: coleccionGuardada,
                        token: ctoken
                    });
                }); 
            });
        }
    })
    } 
  });

  app.get('/api/auth/me',auth, (req, res, next) =>{
    db.user.findOne({_id:req.user.id}, (err, user) => {
        if(err) return next(err);
        res.json({
            result: 'OK',
            user: user
        });
    });
}); 

app.get('/api/auth',auth,(req, res, next) =>{
    db.user.find((err, colecciones) => {
        if(err) return next(err);
        res.json({
            result: 'OK',
            Usuarios: colecciones
        });
    });
}); 

//_______________________________user______________________________________


app.get('/api/user',auth,(req, res, next) =>{
    db.user.find((err, colecciones) => {
        if(err) return next(err);
        res.json({
            result: 'OK',
            Usuarios: colecciones
        });
    });
}); 



app.get('/api/user/:id',auth, (req, res, next) =>{
    db.user.findOne({_id:id(req.params.id)}, (err, user) => {
        if(err) return next(err);
        res.json(user);
    });
});

app.post('/api/user', auth, (req,res, next) => {
    const elemento = req.body; 
    console.log(elemento);
  

    if (!elemento.nombre) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <nombre>' 
      }); 
    } else if(!elemento.email) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <email>' 
      }); 
    }else if(!elemento.pass) { 
      res.status(400).json ({ 
      error: 'Bad data', 
      description: 'Se precisa al menos un campo <pass>' 
      }); 
    } else { 
      db.user.findOne({ email: elemento.email }, (err, usuario)=>{
        if(usuario){
            res.status(400).json({error: 'Duplicated User',
            description: 'EL usuario que se ha querido registrar ya existe'});
        }else{
            PassService.encriptaPassword(elemento.pass)
            .then(passEnc => {
               
               const usuario = {
                    email: elemento.email,
                    name: elemento.nombre,
                    pass: passEnc,
                    signUpDate: moment().unix(),
                    lastLogin: moment().unix()
                };
                db.user.save(usuario, (err, coleccionGuardada) => { 
                    if(err) return next(err);
                    const ctoken = TokenService.creaToken(usuario);
                    res.json({
                        result: 'OK',
                        user: coleccionGuardada,
                        token: ctoken
                    });
                }); 
            });
        }
    })
    } 
});

app.put('/api/user/:id', auth, (req,res, next) =>{
    const elemento = req.params.id; 
    const cosas=req.body;
    if(cosas.pass){
    PassService.encriptaPassword(cosas.pass).then(passEnc =>{
        cosas.lastLogin=moment().unix();
        cosas.pass=passEnc;
        db.user.update({_id:id(elemento)}, {$set: cosas}, {safe: true, multi: false}, (err, elementoModif) =>{
            if(err) return next(err);
            res.json({
                result: 'OK',                                                                                                                                                                                                                                                                       
                user: elementoModif
            });
        });
    });
    }else{
        cosas.lastLogin=moment().unix();
        db.user.update({_id:id(elemento)}, {$set: cosas}, {safe: true, multi: false}, (err, elementoModif) =>{
            if(err) return next(err);
            res.json({
                result: 'OK',                                                                                                                                                                                                                                                                       
                user: elementoModif
            });
        });
    }
});

app.delete('/api/user/:id', auth, (req, res, next)=>{
    let elementoID = req.params.id;

    db.user.remove({_id: id(elementoID)}, (err, resultado) =>{
        if(err) return next(err);
        res.json({
            result: 'OK',
            response: resultado
        });
    });
});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => { 
    console.log(`SEC WS API REST CRUD ejecutándose en https://localhost:${port}/api/products`);
});