'use strict'

const port = process.env.PORT || 3000; 
const express = require('express'); 
const logger = require('morgan');
const mongojs = require('mongojs'); 
const fs=require('fs');

const OPTIONS_HTTPS ={
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
};

const moment = require('moment');
const PassService = require('./services/pass.service');
const TokenService = require('./services/token.service');

const usuario={
    _id: '123456789',
    email: 'jpi9@alu.ua.es',
    displayName: 'Joan Pascual',
    password: 'password1234',
    signUpDate: moment().unix(),
    lastLogin: moment().unix()
};
const token = TokenService.creaToken(usuario);

const app = express();
const https = require('https');


var db = mongojs("SD-p5");
var id = mongojs.ObjectID;

var helmet = require('helmet');




//Middleware
app.use(helmet());
const cors = require('cors'); 
// Middlewares
var allowCrossTokenHeader = (req, res, next) => { 
 res.header("Access-Control-Allow-Headers", "*"); 
return next(); 
}; 
var allowCrossTokenOrigin = (req, res, next) => { 
 res.header("Access-Control-Allow-Origin", "*"); 
return next(); 
}; 
app.use(cors()); 
app.use(allowCrossTokenHeader); 
app.use(allowCrossTokenOrigin);
var auth = (req, res, next) =>{
    return next();
    if(req.headers.token === "password1234"){
        return next();
    }else{
        return next(new Error("No autorizado"));
    };
};

// Declaramos los middleware 
app.use(logger('dev')); // probar con: tiny, short, dev, common, combined
app.use(express.urlencoded({extended:false}));
app.use(express.json());
//app.use(cors);
//app.use(allowCrossTokenHeader);
//app.use(allowCrossTokenOrigin);

app.param("coleccion", (req, res, next, coleccion) =>{

    console.log('param /api/:coleccion');
    console.log('colección: ', coleccion);
    req.collection = db.collection(coleccion);
    return next();
});

// Implementamos el API RESTFul a través de los métodos 


app.get('/api', (req, res, next) =>{
    console.log('GET /api');
    console.log(req.params);
    console.log(req.collection);
    db.getCollectionNames((err, colecciones) => {
        if(err) return next(err);
        res.json(colecciones);
    });
}); 


app.get('/api/:coleccion', (req, res, next) =>{
    req.collection.find((err, colecciones) => {
        if(err) return next(err);
        res.json(colecciones);
    });
}); 



app.get('/api/:coleccion/:id', (req, res, next) =>{
    req.collection.findOne({_id: id(req.params.id)}, (err, elemento) => {
        if(err) return next(err);
        res.json({
            objeto: elemento,
        nombre: elemento.nombre});
    });
});

app.post('/api/:coleccion', auth, (req,res, next) => {
    const elemento= req.body;

    if(!elemento.Title){
        res.status(400).json({
            error: 'Bad data',
            description: 'Se precisa de al menos un campo <nombre>'
        });
    }else{
        req.collection.save(elemento, (err, coleccionGuardada) => {
            if(err) return next(err);
            res.json(coleccionGuardada);
        });
    }
});

app.put('/api/:coleccion/:id', auth, (req,res, next) =>{
    let elementoID = req.params.id;
    let elementoNuevo = req.body;
    req.collection.update ({_id: id(elementoID)}, {$set: elementoNuevo}, {safe: true, multi: false}, (err, elementoModif) =>{
        if(err) return next(err);
        res.json(elementoModif);
    });
});

app.delete('/api/:coleccion/:id', auth, (req, res, next)=>{
    let elementoID = req.params.id;

    req.collection.remove({_id: id(elementoID)}, (err, resultado) =>{
        if(err) return next(err);
        res.json(resultado);
    });
});

https.createServer( OPTIONS_HTTPS, app).listen(port, () => { 
    console.log(`SEC WS API REST CRUD ejecutándose en https://localhost:${port}/api/`);
   });

// Lanzamos nuestro servicio API 
/*
app.listen(port, () => { 
 console.log(`API REST CRUD ejecutándose en http://localhost:${port}/api/products`);
});
*/